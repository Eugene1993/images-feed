//
//  BoardsInteractorTests.swift
//  images-feedTests
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import XCTest
@testable import images_feed

class InteractorTests: XCTestCase {
    
    func testsThatLoadBoardsCallsBoardsServiceFetchBoards() {
        let boardsService = BoardsServiceMock()
        let interactor = BoardsInteractor(boardsService: boardsService)
        
        interactor.loadBoards()
        
        assert(boardsService.fetchBoardsFired)
    }
}
