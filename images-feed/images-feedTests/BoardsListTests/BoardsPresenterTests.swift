//
//  BoadsPresenterTests.swift
//  images-feedTests
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import XCTest
@testable import images_feed

class BoardsPresenterTests: XCTestCase {
    
    func testsThatDidCloseCallsRouterDismiss() {
        let board = Board(name: "b", title: "random", pages: 10)
        let presenter = BoardsPresenter(currentBoard: board)
        let interactor = BoardsInteractorInputMock()
        let view = BoardsViewInputMock()
        let router = BoardsRouterInputMock()
        presenter.interactor = interactor
        presenter.currentBoard = board
        presenter.view = view
        presenter.router = router
        
        presenter.didClose()
        
        assert(router.dismissCalled)
    }
    
    func testsThatDidSelectBoardCallsRouterOpenBoard() {
        let board = Board(name: "b", title: "random", pages: 10)
        let presenter = BoardsPresenter(currentBoard: board)
        let interactor = BoardsInteractorInputMock()
        let view = BoardsViewInputMock()
        let router = BoardsRouterInputMock()
        presenter.interactor = interactor
        presenter.currentBoard = board
        presenter.view = view
        presenter.router = router
        
        presenter.didSelectBoard(boardViewModel: BoardViewModel(board: Board(name: "b", title: "random", pages: 10)))
        
        assert(router.openBoardCalled)
    }
}
