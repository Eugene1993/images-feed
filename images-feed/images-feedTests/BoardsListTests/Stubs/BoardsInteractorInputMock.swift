//
//  BoardsInteractorInputMock.swift
//  images-feedTests
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

import XCTest
@testable import images_feed

class BoardsInteractorInputMock: BoardsInteractorInput {
    private(set) var loadBoardsFired = false
    func loadBoards() {
        loadBoardsFired = true
    }
}
