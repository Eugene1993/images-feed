//
//  BoardsServiceMock.swift
//  images-feedTests
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
@testable import images_feed

class BoardsServiceMock: BoardsServiceProtocol {
    private(set) var fetchBoardsFired = false
    private(set) var cancelFired = false
    
    func fetchBoards(completion: @escaping ([Board], Error?) -> Void) {
        let boards = [Board(name: "b", title: "random", pages: 10)]
        fetchBoardsFired = true
        completion(boards, nil)
    }
    
    func cancel() {
        cancelFired = true
    }
}
