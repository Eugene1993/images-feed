//
//  BoardsRouterTransitionListenerMock.swift
//  images-feedTests
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
@testable import images_feed

class TransitionListenerMock: BoardsRouterTransitionListener {
    private(set) var didOpenBoardsFired = false
    
    func didOpenBoard(board: Board) {
        didOpenBoardsFired = true
    }
}
