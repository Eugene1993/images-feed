//
//  BoardsRouterInputMock.swift
//  images-feedTests
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

import XCTest
@testable import images_feed

class BoardsRouterInputMock: BoardsRouterInput {
    private(set) var dismissCalled = false
    private(set) var openBoardCalled = false
    
    func dismiss() {
        dismissCalled = true
    }
    
    func openBoard(board: Board) {
        openBoardCalled = true
    }
}
