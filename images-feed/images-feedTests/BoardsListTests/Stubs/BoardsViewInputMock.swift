//
//  BoardsViewInput.swift
//  images-feedTests
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

import XCTest
@testable import images_feed

class BoardsViewInputMock: BoardsViewInput {
    private(set) var showBoardsFired = false
    
    func showBoards(viewModels: [BoardViewModel], currentBoard: Board?) {
        showBoardsFired = true
    }
    
    func showError(error: String) {
        
    }
    
    func setLoading(isLoading: Bool) {
        
    }
}
