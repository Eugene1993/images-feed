//
//  Colors.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let appDarkGray = UIColor(red: 115.0/255, green: 115.0/255, blue: 118.0/255, alpha: 1.0)
    static let appLightGray = UIColor(red: 157.0/255, green: 158.0/255, blue: 159.0/255, alpha: 1.0)
    static let appRed = UIColor(red: 245.0/255, green: 0.0/255, blue: 87.0/255, alpha: 1.0)
    static let appYellow = UIColor(red: 202.0/255, green: 169.0/255, blue: 56.0/255, alpha: 1.0)
    static let appBlack = UIColor(red: 23.0/255, green: 23.0/255, blue: 29.0/255, alpha: 1.0)
}
