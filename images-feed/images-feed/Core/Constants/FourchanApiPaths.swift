//
//  FourchanApiPaths.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class FourchanApiPaths {
    static let fourchanThreadsAndBoardsUrl = "https://a.4cdn.org"
    static let fourchanFilesUrl = "https://i.4cdn.org"
    static let boardsUrl = "\(fourchanThreadsAndBoardsUrl)/boards.json"
    static let boardCatalogUrl = "\(fourchanThreadsAndBoardsUrl)/%@/catalog.json"
    static let boardPageUrl = "\(fourchanThreadsAndBoardsUrl)/%@/%d.json"
    static let threadsUrl = "\(fourchanThreadsAndBoardsUrl)/%@/threads.json"
    static let threadUrl = "\(fourchanThreadsAndBoardsUrl)/%@/%@.json"
    static let imageUrl = "\(fourchanFilesUrl)/%@/%@%@"
    static let thumbnailsUrl = "\(fourchanFilesUrl)/%@/%@s.jpg"
}
