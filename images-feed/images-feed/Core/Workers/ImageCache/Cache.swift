//
//  Cache.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class ImageCache: ImageCacheProtocol {
    private let cache: NSCache<NSString, Image>
    
    init(maxCacheSizeInBytes: Int, maxCountOfCachedObjects: Int) {
        self.cache = NSCache()
        self.cache.totalCostLimit = maxCacheSizeInBytes
        self.cache.countLimit = maxCountOfCachedObjects
    }
    
    func set(item: Image, forKey: String) {
        cache.setObject(item, forKey: NSString(string: forKey), cost: item.data.count)
    }
    
    func get(key: String) -> Image? {
        return cache.object(forKey: NSString(string: key))
    }
    
    func removeAll() {
        cache.removeAllObjects()
    }
    
    func removeItem(forKey: String) {
        cache.removeObject(forKey: NSString(string: forKey))
    }
}
