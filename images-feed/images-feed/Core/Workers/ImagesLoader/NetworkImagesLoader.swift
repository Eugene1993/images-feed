//
//  NetworkImageLoader.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

struct LoadRequest {
    let timestamp: Date
    let request: HttpRequestProtocol
}

final class NetworkImagesLoader: ImagesLoaderProtocol {
    private let serialAccessQueue = OperationQueue()
    private let imageService: ImageServiceProtocol
    private var previewRequests = [ImageDescription: LoadRequest]()
    private var imageRequests = [ImageDescription: LoadRequest]()
    private let imagesCache: ImageCache
    private let previewsCache: ImageCache
    private let simultaneousRequestsLimit = 5
    
    weak var delegate: ImageLoaderDelegate?
    
    init(imageService: ImageServiceProtocol) {
        self.imageService = imageService
        self.serialAccessQueue.maxConcurrentOperationCount = 1
        self.serialAccessQueue.qualityOfService = .userInitiated
        self.imagesCache = ImageCache(maxCacheSizeInBytes: 100 * 1024 * 1024, maxCountOfCachedObjects: 150)
        self.previewsCache = ImageCache(maxCacheSizeInBytes: 10 * 1024 * 1024, maxCountOfCachedObjects: 150)
    }
    
    func startLoadingImage(imageDescription: ImageDescription) {
        self.serialAccessQueue.addOperation {
            let previewImageUrl = imageDescription.fourchanImageThumbnailsUrl
            let imageUrl = imageDescription.fourchanImageUrl
            
            if let cachedImage = self.imagesCache.get(key: imageUrl) {
                guard let delegate = self.delegate else {
                    return
                }
                delegate.didLoadImage(image: cachedImage)
                return
            }
            
            if self.imageRequests[imageDescription] != nil {
                return
            }
            
            let imageRequest = self.createImageDownloadRequest(imageDescription: imageDescription)
            if self.imageRequests.count >= self.simultaneousRequestsLimit {
                self.removeOldestRequest()
            }
            self.imageRequests[imageDescription] = LoadRequest(timestamp: Date(), request: imageRequest)
            
            if let cachedPreview = self.previewsCache.get(key: previewImageUrl) {
                
                guard let delegate = self.delegate else {
                    return
                }
                
                delegate.didLoadImagePreview(previewImage: cachedPreview)
                return
            }
            
            if self.previewRequests[imageDescription] != nil {
                return
            }
            
            let previewRequest = self.createImagePreviewDownloadRequest(imageDescription: imageDescription)
            self.previewRequests[imageDescription] = LoadRequest(timestamp: Date(), request: previewRequest)
        }
    }
    
    func cancelLoadingImage(imageDescription: ImageDescription) {
        self.serialAccessQueue.addOperation {
            guard let loadRequest = self.imageRequests[imageDescription] else {
                return
            }
            loadRequest.request.cancel()
            self.delegate?.didCancelLoadingImage(imageDescription: imageDescription)
        }
    }
    
    func cancelLoadingPreview(imageDescription: ImageDescription) {
        self.serialAccessQueue.addOperation {
            guard let loadRequest = self.previewRequests[imageDescription] else {
                return
            }
            loadRequest.request.cancel()
        }
    }
    
    func cancelLoadingImages() {
        self.serialAccessQueue.addOperation {
            for loadRequest in self.imageRequests {
                loadRequest.value.request.cancel()
                guard let delegate = self.delegate else {
                    continue
                }
                delegate.didCancelLoadingImage(imageDescription: loadRequest.key)
            }
            
            for loadRequest in self.previewRequests {
                loadRequest.value.request.cancel()
            }
            self.imageRequests.removeAll()
            self.previewRequests.removeAll()
        }
    }
    
    func clearCache() {
        self.serialAccessQueue.addOperation {
            self.imagesCache.removeAll()
            self.previewsCache.removeAll()
        }
    }
    
    private func removeOldestRequest() {
        let oldestRequestKey = self.imageRequests.min { (first, second) -> Bool in
            let (_, firstValue) = first
            let (_, secondValue) = second
            return firstValue.timestamp < secondValue.timestamp
        }
        
        guard let key = oldestRequestKey?.key, let request = oldestRequestKey?.value.request else {
            return
        }
        
        request.cancel()
        imageRequests.removeValue(forKey: key)
        self.delegate?.didCancelLoadingImage(imageDescription: key)
    }
    
    private func createImageDownloadRequest(imageDescription: ImageDescription) -> HttpRequestProtocol {
        return self.imageService.fetchImage(description: imageDescription, completion: { (image, error) in
            defer {
                self.serialAccessQueue.addOperation {
                    self.imageRequests.removeValue(forKey: imageDescription)
                }
            }
            
            if error != nil {
                return
            }
            
            guard let delegate = self.delegate, let image = image else {
                return
            }
            
            let url = imageDescription.fourchanImageUrl
            self.imagesCache.set(item: image, forKey: url)
            delegate.didLoadImage(image: image)
        })
    }
    
    private func createImagePreviewDownloadRequest(imageDescription: ImageDescription) -> HttpRequestProtocol {
        return self.imageService.fetchImageThumbnails(description: imageDescription, completion: { (image, error) in
            
            defer {
                self.serialAccessQueue.addOperation {
                    self.previewRequests.removeValue(forKey: imageDescription)
                }
            }
            
            if error != nil {
                return
            }
            
            guard let delegate = self.delegate, let image = image else {
                return
            }
            
            let url = imageDescription.fourchanImageThumbnailsUrl
            self.previewsCache.set(item: image, forKey: url)
            delegate.didLoadImagePreview(previewImage: image)
        })
    }
}
