//
//  ImagesLoaderProtocol.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImagesLoaderProtocol: class {
    var delegate: ImageLoaderDelegate? { get set }
    func startLoadingImage(imageDescription: ImageDescription)
    func cancelLoadingImage(imageDescription: ImageDescription)
    func cancelLoadingPreview(imageDescription: ImageDescription)
    func cancelLoadingImages()
    func clearCache()
}
