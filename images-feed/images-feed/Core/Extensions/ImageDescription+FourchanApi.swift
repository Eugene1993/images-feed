//
//  PictureDescription+FourchanApi.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

extension ImageDescription: Hashable {
    var fourchanImageUrl: String {
        get {
            return String(format: FourchanApiPaths.imageUrl, self.boardId, self.name, self.extention)
        }
    }
    
    var fourchanImageThumbnailsUrl: String {
        get {
            return String(format: FourchanApiPaths.thumbnailsUrl, self.boardId, self.name)
        }
    }
    
    static func == (lhs: ImageDescription, rhs: ImageDescription) -> Bool {
        return lhs.name == rhs.name && lhs.extention == rhs.extention && lhs.boardId == rhs.boardId
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.name)
        hasher.combine(self.extention)
        hasher.combine(self.boardId)
    }
}
