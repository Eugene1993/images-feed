//
//  AdvertisementServiceProtocol.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol AdvertisementServiceProtocol: class {
    var listener: AdvertisementServiceListener? { get set }
    func start()
    func stop()
}
