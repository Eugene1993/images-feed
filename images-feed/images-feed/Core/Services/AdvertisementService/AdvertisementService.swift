//
//  AdvertisementLinksService.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class AdvertisementService: AdvertisementServiceProtocol {
    private var timer: Timer?
    private let nextAdvertisementTimeoutInSeconds: TimeInterval
    private let availableAdvertisementLinks: [String]
    
    weak var listener: AdvertisementServiceListener?
    
    init(delay nextAdvertisementDelay: TimeInterval) {
        self.nextAdvertisementTimeoutInSeconds = nextAdvertisementDelay
        self.availableAdvertisementLinks = ["https://google.com/", "https://www.apple.com/", "https://yandex.ru/"]
    }
    
    func start() {
        stop()
        timer = Timer.scheduledTimer(timeInterval: self.nextAdvertisementTimeoutInSeconds, target: self, selector: #selector(onTimeout), userInfo: nil, repeats: true)
    }
    
    func stop() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc private func onTimeout() {
        if let randomLink = availableAdvertisementLinks.randomElement() {
            listener?.didReceiveAdvertisementLink(link: randomLink)
        }
    }
}
