//
//  ThreadsService.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class FourchanThreadsService: ThreadsServiceProtocol {
    let httpClient: HttpClientProtocol
    private var currentRequest: HttpRequestProtocol?
    
    init(httpClient: HttpClientProtocol) {
        self.httpClient = httpClient
    }
    
    func fetchThreads(boardId: String, pageNumber: Int, completion: @escaping ([Thread], Error?) -> Void) {
        let url = FourchanApiPaths.boardPageUrl
        let boardThreadsUrl = String(format: url, boardId, pageNumber)
        currentRequest = httpClient.makeRequest(url: boardThreadsUrl)
        currentRequest?.getDataResponse { (data, error) in
            var pagePosts = [Thread]()
            
            defer {
                completion(pagePosts, error)
            }
            
            guard let data = data else {
                return
            }
            
            guard let json = try? JSON(data: data) else {
                return
            }
            
            let threads = json["threads"].arrayValue
            for threadPosts in threads {
                let posts = threadPosts["posts"].arrayValue
                for post in posts {
                    guard let thread = self.convertJsonToThread(threadJson: post, boardId: boardId) else {
                        continue
                    }
                    pagePosts.append(thread)
                }
            }
        }
    }
    
    func cancel() {
        currentRequest?.cancel()
    }
    
    private func convertJsonToThread(threadJson: JSON, boardId: String) -> Thread? {
        let extention = threadJson["ext"].stringValue
        
        guard ContentFormat(rawValue: extention) != nil else {
            return nil
        }
        
        let comment = threadJson["com"].stringValue
        let threadNumber = threadJson["no"].int32Value
        let repliesCount = threadJson["replies"].int32Value
        let imagesCount = threadJson["images"].int32Value
        let filename = threadJson["tim"].stringValue
        
        let image = ImageDescription(name: filename, extention: extention, boardId: boardId)
        let thread = Thread(boardId: boardId, comment: comment, replies: repliesCount, images: imagesCount, threadNumber: threadNumber, image: image)
        return thread
    }
}
