//
//  ContentFormat.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

enum ContentFormat: String {
    case png = ".png"
    case jpg = ".jpg"
}
