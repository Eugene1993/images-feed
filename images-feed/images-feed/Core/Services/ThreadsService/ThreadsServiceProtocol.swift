//
//  ThreadsServiceProtocol.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ThreadsServiceProtocol {
    func fetchThreads(boardId: String, pageNumber: Int, completion: @escaping ([Thread], Error?) -> Void)
    func cancel()
}
