//
//  ImagesServiceProtocol.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImageServiceProtocol {
    func fetchImage(description: ImageDescription, completion: @escaping (Image?, Error?) -> Void) -> HttpRequestProtocol
    func fetchImageThumbnails(description: ImageDescription, completion: @escaping (Image?, Error?) -> Void) -> HttpRequestProtocol
}
