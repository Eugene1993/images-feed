//
//  ImageService.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import Alamofire

final class FourchanImageService: ImageServiceProtocol {
    let httpClient: HttpClientProtocol
    
    init(httpClient: HttpClientProtocol) {
        self.httpClient = httpClient
    }
    
    func fetchImage(description: ImageDescription, completion: @escaping (Image?, Error?) -> Void) -> HttpRequestProtocol {
        let imageUrl = description.fourchanImageUrl
        return fetchImage(imageUrl: imageUrl, description: description, completion: completion)
    }
    
    func fetchImageThumbnails(description: ImageDescription, completion: @escaping (Image?, Error?) -> Void) -> HttpRequestProtocol {
        let imageUrl = description.fourchanImageThumbnailsUrl
        return fetchImage(imageUrl: imageUrl, description: description, completion: completion)
    }
    
    private func fetchImage(imageUrl: String, description: ImageDescription, completion: @escaping (Image?, Error?) -> Void) -> HttpRequestProtocol {
        let request = httpClient.makeRequest(url: imageUrl)
        request.getDataResponse { (data, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let data = data else {
                completion(nil, error)
                return
            }
            
            let image = Image(description: description, data: data)
            completion(image, nil)
        }
        return request
    }
}
