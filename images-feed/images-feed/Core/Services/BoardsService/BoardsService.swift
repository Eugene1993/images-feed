//
//  FourchanBoardsService.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class FourchanBoardsService: BoardsServiceProtocol {
    private let httpClient: HttpClientProtocol
    private var currentRequest: HttpRequestProtocol?
    private let nsfwBoards = ["s", "hc", "hm", "h", "e", "u", "d", "y", "t", "hr", "gif", "aco", "r"]
    
    init(httpClient: HttpClientProtocol) {
        self.httpClient = httpClient
    }
    
    func fetchBoards(completion: @escaping ([Board], Error?) -> Void) {
        cancel()
        let url = FourchanApiPaths.boardsUrl
        currentRequest = httpClient.makeRequest(url: url)
        currentRequest?.getDataResponse(completion: { (data, error) in
            var boards = [Board]()
            defer {
                completion(boards, error)
            }
            
            if let data = data {
                do {
                    let json = try JSON(data: data)
                    let boardsJson = json["boards"].arrayValue
                    for boardJson in boardsJson {
                        if let board = self.convertJsonToBoard(boardJson: boardJson) {
                            boards.append(board)
                        }
                    }
                } catch {
                }
            }
        })
    }
    
    func cancel() {
        currentRequest?.cancel()
    }
    
    private func convertJsonToBoard(boardJson: JSON) -> Board? {
        let boardName = boardJson["board"].stringValue
        if nsfwBoards.contains(boardName) {
            return nil
        }
        let boardTitle = boardJson["title"].stringValue
        let pagesCount = boardJson["pages"].intValue
        let board = Board(name: boardName, title: boardTitle, pages: pagesCount)
        return board
    }
}
