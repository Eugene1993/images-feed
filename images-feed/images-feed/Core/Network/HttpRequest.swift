//
//  AlamofireHttpRequest.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireHttpRequest: HttpRequestProtocol {
    let request: DataRequest
    
    init(request: DataRequest) {
        self.request = request
    }
    
    func getDataResponse(completion: @escaping (Data?, Error?) -> Void) {
        request.responseData { (dataResponse) in
            let data = dataResponse.data
            let error = dataResponse.error
            completion(data, error)
        }
    }
    
    func cancel() {
        request.cancel()
    }
}
