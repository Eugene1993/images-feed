//
//  AlamofireHttpClient.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireHttpClient: HttpClientProtocol {
    func makeRequest(url: String) -> HttpRequestProtocol {
        let request = Alamofire.request(url)
        return AlamofireHttpRequest(request: request)
    }
}
