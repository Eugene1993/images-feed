//
//  HttpRequestProtocol.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol HttpRequestProtocol {
    func getDataResponse(completion: @escaping (Data?, Error?) -> Void)
    func cancel()
}
