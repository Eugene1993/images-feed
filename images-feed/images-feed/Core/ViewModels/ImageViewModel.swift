//
//  ImageViewModel.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class ImageViewModel: ViewModelProtocol {
    let imageDescription: ImageDescription
    var isViewed: Bool = false
    
    init(imageDescription: ImageDescription) {
        self.imageDescription = imageDescription
    }
}
