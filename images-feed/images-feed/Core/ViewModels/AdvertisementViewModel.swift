//
//  AdvertisementViewModel2.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class AdvertisementViewModel: ViewModelProtocol {
    let link: String
    
    init(link: String) {
        self.link = link
    }
}
