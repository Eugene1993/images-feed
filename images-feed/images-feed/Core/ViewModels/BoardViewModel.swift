//
//  BoardViewModel.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class BoardViewModel {
    let board: Board
    
    var name: String {
        get {
            return "/\(board.name)/"
        }
    }
    
    var title: String {
        get {
            return board.title
        }
    }
    
    init(board: Board) {
        self.board = board
    }
}
