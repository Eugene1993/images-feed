//
//  File.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class ImageDescription {
    let name: String
    let extention: String
    let boardId: String
    
    init(name: String, extention: String, boardId: String) {
        self.name = name
        self.extention = extention
        self.boardId = boardId
    }
}
