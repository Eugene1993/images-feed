//
//  Picture.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class Image {
    let description: ImageDescription
    let data: Data
    
    init(description: ImageDescription, data: Data) {
        self.description = description
        self.data = data
    }
}
