//
//  Board.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class Board {
    let name: String
    let title: String
    let pages: Int
    
    init(name: String, title: String, pages: Int) {
        self.name = name
        self.title = title
        self.pages = pages
    }
}
