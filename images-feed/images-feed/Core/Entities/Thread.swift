//
//  Thread.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class Thread {
    let boardId: String
    let comment: String
    let repliesCount: Int32
    let imagesCount: Int32
    let threadNumber: Int32
    var image: ImageDescription
    
    init(boardId: String, comment: String, replies: Int32, images: Int32, threadNumber: Int32, image: ImageDescription) {
        self.boardId = boardId
        self.comment = comment
        self.repliesCount = replies
        self.imagesCount = images
        self.threadNumber = threadNumber
        self.image = image
    }
}
