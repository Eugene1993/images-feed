//
//  ImageFeedRouter.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class ImagesFeedRouter: ImagesFeedRouterInput {
    weak var controller: ImagesFeedViewController?
    
    init(controller: ImagesFeedViewController) {
        self.controller = controller
    }
    
    func showBoards(transitionListener: BoardsRouterTransitionListener, currentBoard: Board?) {
        let httpClient = AlamofireHttpClient()
        let boardsService = FourchanBoardsService(httpClient: httpClient)
        let boardsModule = BoardsModule(transitionListener: transitionListener, boardsService: boardsService, currentBoard: currentBoard)
        let boardsView = boardsModule.view
        let navController = UINavigationController(rootViewController: boardsView)
        navController.modalTransitionStyle = .crossDissolve
        controller?.present(navController, animated: true, completion: nil)
    }
}
