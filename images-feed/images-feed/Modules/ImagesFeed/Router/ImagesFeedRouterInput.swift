//
//  ImagesFeedRouterInput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImagesFeedRouterInput {
    func showBoards(transitionListener: BoardsRouterTransitionListener, currentBoard: Board?)
}
