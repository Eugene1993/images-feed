//
//  ImagesFeedInteractor.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class ImagesFeedInteractor: ImagesFeedInteractorInput {
    weak var presenter: ImagesFeedInteractorOutput?
    private let threadsService: ThreadsServiceProtocol
    private let boardsService: BoardsServiceProtocol
    private let imagesLoader: ImagesLoaderProtocol
    private var threads: [Thread]?
    private var imageLoadHandlers = [ImageDescription: (Image, Bool) -> Void]()
    private let serialAccessQueue = OperationQueue()
    private var currentBoard: Board?
    private var currentPage = 1

    init(imagesLoader: ImagesLoaderProtocol, threadService: ThreadsServiceProtocol, boardsService: BoardsServiceProtocol) {
        self.serialAccessQueue.qualityOfService = .userInteractive
        self.serialAccessQueue.maxConcurrentOperationCount = 1
        self.imagesLoader = imagesLoader
        self.threadsService = threadService
        self.boardsService = boardsService
        self.imagesLoader.delegate = self
    }
    
    func didOpenBoard(board: Board) {
        reset()
        presenter?.setLoading(isLoading: true)
        currentBoard = board
        threadsService.fetchThreads(boardId: board.name, pageNumber: currentPage) { (threads, error) in
            self.threads = threads
            self.presenter?.showFeed(threads: threads, board: board)
            if let error = error {
                self.presenter?.showError(error: error.localizedDescription)
            }
        }
    }
    
    func fetchFeedImage(imageDescription: ImageDescription, completion: @escaping (Image, Bool) -> Void) {
        self.imageLoadHandlers[imageDescription] = completion
        self.imagesLoader.startLoadingImage(imageDescription: imageDescription)
    }
    
    func cancelLoadingImages() {
        serialAccessQueue.addOperation {
            self.imagesLoader.cancelLoadingImages()
        }
    }
    
    func didLoad() {
        reset()
        self.boardsService.fetchBoards { (boards, error) in
            if let error = error {
                self.presenter?.setLoading(isLoading: false)
                self.presenter?.showError(error: error.localizedDescription)
            }
            if let firstBoard = boards.first {
                self.didOpenBoard(board: firstBoard)
            }
        }
    }
    
    func loadMoreThreads() {
        guard let currentBoard = currentBoard else {
            return
        }
        if currentPage == currentBoard.pages {
            return
        }
        
        presenter?.setLoading(isLoading: true)
        self.threadsService.fetchThreads(boardId: currentBoard.name, pageNumber: currentPage, completion: { (threads, error) in
            defer {
                self.presenter?.setLoading(isLoading: false)
            }
            if let error = error {
                self.presenter?.showError(error: error.localizedDescription)
                return
            }
            self.currentPage += 1
            self.threads?.append(contentsOf: threads)
            self.presenter?.appendNewThreads(threads: threads)
        })
    }
    
    private func reset() {
        serialAccessQueue.isSuspended = true
        imageLoadHandlers.removeAll()
        serialAccessQueue.cancelAllOperations()
        imagesLoader.cancelLoadingImages()
        imagesLoader.clearCache()
        serialAccessQueue.isSuspended = false
        boardsService.cancel()
        threadsService.cancel()
    }
}

extension ImagesFeedInteractor: ImageLoaderDelegate {
    func didLoadImage(image: Image) {
        serialAccessQueue.addOperation {
            guard let handler = self.imageLoadHandlers[image.description] else {
                return
            }
            handler(image, false)
            self.imageLoadHandlers.removeValue(forKey: image.description)
        }
    }
    
    func didLoadImagePreview(previewImage: Image) {
        serialAccessQueue.addOperation {
            guard let handler = self.imageLoadHandlers[previewImage.description] else {
                return
            }
            handler(previewImage, true)
        }
    }
    
    func didCancelLoadingImage(imageDescription: ImageDescription) {
        serialAccessQueue.addOperation {
            self.imageLoadHandlers.removeValue(forKey: imageDescription)
        }
    }
}
