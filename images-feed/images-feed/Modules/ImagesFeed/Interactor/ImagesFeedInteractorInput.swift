//
//  ImagesFeedInteractorInput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImagesFeedInteractorInput: BoardsRouterTransitionListener {
    func fetchFeedImage(imageDescription: ImageDescription, completion: @escaping (Image, Bool) -> Void)
    func didLoad()
    func cancelLoadingImages()
    func loadMoreThreads()
}
