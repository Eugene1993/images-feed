//
//  ImagesFeedInteractorOutput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImagesFeedInteractorOutput: class {
    func showFeed(threads: [Thread], board: Board)
    func appendNewThreads(threads: [Thread])
    func setLoading(isLoading: Bool)
    func showError(error: String)
}
