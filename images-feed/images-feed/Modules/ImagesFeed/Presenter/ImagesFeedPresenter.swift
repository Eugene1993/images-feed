//
//  ImagesFeedViewPresenter.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class ImagesFeedPresenter: ImagesFeedViewOutput, ImagesFeedInteractorOutput {
    weak var view: ImagesFeedViewInput?
    var interactor: ImagesFeedInteractorInput?
    var router: ImagesFeedRouterInput?
    private var currentBoard: Board?
    
    func showFeed(threads: [Thread], board: Board) {
        currentBoard = board
        let viewModels = threads.map { (thread) -> ViewModelProtocol in
            return ImageViewModel(imageDescription: thread.image)
        }
        
        view?.showFeed(viewModels: viewModels)
        view?.setBoardName(title: "\(board.name)")
        view?.setLoading(isLoading: false)
    }
    
    func appendNewThreads(threads: [Thread]) {
        let viewModels = threads.map { (thread) -> ImageViewModel in
            return ImageViewModel(imageDescription: thread.image)
        }
        self.view?.appendNewImages(newViewModels: viewModels)
    }
    
    func didShow(imageFeedPage page: ImageFeedPageViewInput, withViewModel viewModel: ImageViewModel) {
        page.clear()
        page.setLoading(isLoading: true)
        interactor?.fetchFeedImage(imageDescription: viewModel.imageDescription, completion: { (image, isPreview) in
            if page.id == image.description.name {
                if isPreview {
                    page.showPreview(preview: image)
                } else {
                    page.showImage(image: image)
                }
            }
        })
    }
    
    func didTapGoToBoardsButton() {
        guard let interactor = interactor else {
            return
        }
        router?.showBoards(transitionListener: interactor, currentBoard: currentBoard)
    }
    
    func didLoad() {
        view?.setLoading(isLoading: true)
        interactor?.didLoad()
    }
    
    func didEnterBackground() {
        view?.setLoading(isLoading: false)
        interactor?.cancelLoadingImages()
    }
    
    func didEnterForeground() {
        
    }
    
    func didScrollToEnd() {
        interactor?.loadMoreThreads()
    }
    
    func setLoading(isLoading: Bool) {
        view?.setLoading(isLoading: isLoading)
    }
    
    func showError(error: String) {
        view?.showError(error: error)
    }
}
