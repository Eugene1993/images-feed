//
//  ImagesFeedAssembly.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class ImagesFeedModule {
    let view: ImagesFeedViewController
    
    init(imagesLoader: ImagesLoaderProtocol, threadsService: ThreadsServiceProtocol, boardsService: BoardsServiceProtocol) {
        let storyboard = UIStoryboard(name: "ImagesFeedViewController", bundle: nil)
        view = storyboard.instantiateViewController(withIdentifier: "ImagesFeedViewController") as! ImagesFeedViewController
        
        let imageFeedPresenter = ImagesFeedPresenter()
        let imageFeedInteractor = ImagesFeedInteractor(imagesLoader: imagesLoader, threadService: threadsService, boardsService: boardsService)
        let imagesFeedRouter = ImagesFeedRouter(controller: view)
        let advertisementService = AdvertisementService(delay: 20)
        let advertisementModule = AdvertisementModule(advertisementViewInput: view, advertisementService: advertisementService)
        
        view.advertisementOutput = advertisementModule.presenter
        view.imagesFeedOutput = imageFeedPresenter
        imageFeedInteractor.presenter = imageFeedPresenter
        imageFeedPresenter.view = view
        imageFeedPresenter.interactor = imageFeedInteractor
        imageFeedPresenter.router = imagesFeedRouter
    }
}
