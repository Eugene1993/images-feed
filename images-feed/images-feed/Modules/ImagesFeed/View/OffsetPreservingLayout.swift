//
//  OffsetPresetvingLayout.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

/// UICollectionViewFlowLayout, сохраняющий позицию видимого элемента при добавлении нового элемента в collectionView
final class OffsetPreservingLayout: UICollectionViewFlowLayout {
    private var additionalOffset: CGFloat = 0.0
    
    override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)
        self.additionalOffset = calculateAdditionalOffset(updateItems: updateItems)
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        if self.scrollDirection != .horizontal {
            return super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
        }
        
        var contentOffset = proposedContentOffset
        contentOffset.x +=  additionalOffset
        additionalOffset = 0.0
        return contentOffset
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        if self.scrollDirection != .horizontal {
            return targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
        }
        var contentOffset = proposedContentOffset
        contentOffset.x += additionalOffset
        additionalOffset = 0.0
        return contentOffset
    }

    /// Возвращает смещение, необходимое для сохранения позиции видимого элемента при вставке нового элемента в collectionView
    private func calculateAdditionalOffset(updateItems: [UICollectionViewUpdateItem]) -> CGFloat {
        if self.scrollDirection != .horizontal {
            return 0
        }
        
        var widthOfInsertedItems: CGFloat = 0.0
        
        for item in updateItems {
            if item.updateAction != .insert {
                continue
            }
            
            let currentIndex = Int(ceil(collectionView!.contentOffset.x / collectionView!.frame.size.width))
            if let itemIndexPath = item.indexPathAfterUpdate {
                if itemIndexPath.row > currentIndex {
                    continue
                }
                if let attrs = layoutAttributesForItem(at: itemIndexPath) {
                    widthOfInsertedItems += attrs.frame.width
                }
            }
        }
        
        return widthOfInsertedItems
    }
}
