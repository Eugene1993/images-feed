//
//  ImageFeedTitleView.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class ImagesFeedTitleView: UIView {
    let unreadPostsLabel: UILabel
    let expandImageView: UIImageView
    let boardIdLabel: UILabel

    override init(frame: CGRect) {
        unreadPostsLabel = UILabel()
        expandImageView = UIImageView()
        boardIdLabel = UILabel();
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        addSubview(unreadPostsLabel)
        addSubview(boardIdLabel)
        addSubview(expandImageView)
        
        unreadPostsLabel.translatesAutoresizingMaskIntoConstraints = false
        unreadPostsLabel.isUserInteractionEnabled = false
        expandImageView.translatesAutoresizingMaskIntoConstraints = false
        expandImageView.isUserInteractionEnabled = false
        boardIdLabel.translatesAutoresizingMaskIntoConstraints = false
        boardIdLabel.isUserInteractionEnabled = false
        
        unreadPostsLabel.layer.cornerRadius = 7.0
        unreadPostsLabel.layer.borderColor = UIColor.appRed.cgColor
        unreadPostsLabel.layer.borderWidth = 1.0
        unreadPostsLabel.textColor = .appRed
        unreadPostsLabel.font = UIFont.boldSystemFont(ofSize: 8)
        unreadPostsLabel.textAlignment = .center
        unreadPostsLabel.text = String(0)
        
        boardIdLabel.textColor = .appLightGray
        boardIdLabel.font = UIFont.boldSystemFont(ofSize: 20)
        boardIdLabel.textAlignment = .left
        
        expandImageView.tintColor = .appDarkGray
        expandImageView.image = UIImage(named: "Expand")
        expandImageView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        expandImageView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        expandImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 2).isActive = true
        expandImageView.leadingAnchor.constraint(equalTo: unreadPostsLabel.trailingAnchor, constant: 8).isActive = true
        
        unreadPostsLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 2).isActive = true
        unreadPostsLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        unreadPostsLabel.widthAnchor.constraint(equalToConstant: 24).isActive = true
        unreadPostsLabel.heightAnchor.constraint(equalToConstant: 14).isActive = true
        
        boardIdLabel.trailingAnchor.constraint(equalTo: unreadPostsLabel.leadingAnchor, constant: -8).isActive = true
        boardIdLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return subviews.first(where: { (view) -> Bool in
            return view.frame.contains(point)
        })
    }
}
