//
//  ImagesFeedViewOutput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImagesFeedViewOutput: class {
    func didShow(imageFeedPage page: ImageFeedPageViewInput, withViewModel viewModel: ImageViewModel)
    func didLoad()
    func didTapGoToBoardsButton()
    func didEnterBackground()
    func didEnterForeground()
    func didScrollToEnd()
}
