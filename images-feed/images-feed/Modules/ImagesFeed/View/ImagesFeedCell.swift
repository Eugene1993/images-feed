//
//  ImagesFeedCell.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class ImagesFeedCell: UICollectionViewCell, ImageFeedPageViewInput {
    var id: String?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageName: UILabel!
    
    override func awakeFromNib() {
        activityIndicator.tintColor = UIColor.appRed
    }
    
    func showImage(image: Image) {
        let data = image.data
        let uiImage = UIImage(data: data)
        DispatchQueue.main.async {
            guard let imageView = self.imageView else {
                return
            }
            imageView.image = uiImage
            self.setLoading(isLoading: false)
        }
    }
    
    func showPreview(preview: Image) {
        let data = preview.data
        let uiImage = UIImage(data: data)
        DispatchQueue.main.async {
            guard let imageView = self.imageView else {
                return
            }
            imageView.image = uiImage
        }
    }
    
    func setName(name: String) {
        self.imageName.text = name
    }
    
    func clear() {
        imageView.image = nil
    }
    
    func setLoading(isLoading: Bool) {
        if isLoading {
            self.activityIndicator.startAnimating()
        } else {
            self.activityIndicator.stopAnimating()
        }
    }
}
