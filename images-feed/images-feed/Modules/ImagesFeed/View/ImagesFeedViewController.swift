//
//  ImagesFeedViewController.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

final class ImagesFeedViewController: UICollectionViewController, ImagesFeedViewInput, AdvertisementViewInput {
    var imagesFeedOutput: ImagesFeedViewOutput?
    var advertisementOutput: AdvertisementViewOutput?
    var viewModels = [ViewModelProtocol]()
    var pagesCount: Int { get { return viewModels.count } }
    
    override func viewDidLoad() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appEnteredBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appBecameActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        self.collectionView.backgroundColor = UIColor.appBlack
        self.collectionView.isPagingEnabled = true
        if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        setupNavigationBar()
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setDefaultStyle(.dark)
        imagesFeedOutput?.didLoad()
        advertisementOutput?.didLoad()
    }
    
    func setBoardName(title: String) {
        guard let titleView = navigationItem.titleView as? ImagesFeedTitleView else {
            navigationItem.title = title
            return
        }
        titleView.boardIdLabel.text = "/\(title)/"
    }
    
    func showFeed(viewModels: [ViewModelProtocol]) {
        self.viewModels = viewModels
        self.collectionView.reloadData()
        let currentOffset = self.collectionView.contentOffset
        self.collectionView.setContentOffset(CGPoint(x: 0, y: currentOffset.y), animated: false)
        updateNotViewedImagesCountTitle()
    }
    
    func appendNewImages(newViewModels: [ImageViewModel]) {
        if newViewModels.count == 0 {
            return
        }
        
        DispatchQueue.main.async {
            let insertIndex = self.pagesCount
            self.viewModels.append(contentsOf: newViewModels)
            var insertIndices = [IndexPath]()
            for i in 0...newViewModels.count - 1 {
                insertIndices.append(IndexPath(row: insertIndex + i, section: 0))
            }
            self.collectionView.insertItems(at: insertIndices)
            self.updateNotViewedImagesCountTitle()
        }
    }
    
    func showAdvertisement(viewModel: AdvertisementViewModel, onPage pageIndex: Int) {
        DispatchQueue.main.async {
            self.viewModels.insert(viewModel, at: pageIndex)
            self.collectionView.insertItems(at: [IndexPath(row: pageIndex, section: 0)])
        }
    }
    
    func setLoading(isLoading: Bool) {
        DispatchQueue.main.async {
            if isLoading {
                SVProgressHUD.show()
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func showError(error: String) {
        DispatchQueue.main.async {
            SVProgressHUD.showError(withStatus: error)
        }
    }
    
    private func setupNavigationBar() {
        guard let navBar = navigationController?.navigationBar else {
            return
        }
        
        navBar.isTranslucent = false
        navBar.barStyle = .blackOpaque
        navBar.barTintColor = .appBlack
        navBar.tintColor = .appRed
        let size = navBar.frame.size
        
        let title = ImagesFeedTitleView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 300, height: size.height)))
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTitleTapped))
        navigationItem.titleView = title
        title.isUserInteractionEnabled = true
        title.addGestureRecognizer(tapRecognizer)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(onRefreshTapped))
    }
    
    private func updateNotViewedImagesCountTitle() {
        var notViewedImagesCount = 0
        self.viewModels.forEach({ (vm) in
            if let imageVm = vm as? ImageViewModel {
                if !imageVm.isViewed {
                    notViewedImagesCount += 1
                }
            }
        })
        
        if let titleView = navigationItem.titleView as? ImagesFeedTitleView {
            titleView.unreadPostsLabel.text = "\(notViewedImagesCount)"
        }
    }
    
    @objc private func onTitleTapped() {
        self.imagesFeedOutput?.didTapGoToBoardsButton()
    }
    
    @objc private func appEnteredBackground() {
        self.imagesFeedOutput?.didEnterBackground()
    }
    
    @objc private func onRefreshTapped() {
        self.imagesFeedOutput?.didLoad()
    }
    
    @objc private func appBecameActive() {
        let currentPageIndex = Int(ceil(self.collectionView.contentOffset.x / self.collectionView.frame.size.width))
        guard let currentPage = self.collectionView.cellForItem(at: IndexPath(row: currentPageIndex, section: 0)) as? ImagesFeedCell else {
            return
        }
        
        guard let vm = self.viewModels[currentPageIndex] as? ImageViewModel else {
            return
        }
        
        imagesFeedOutput?.didShow(imageFeedPage: currentPage, withViewModel: vm)
    }
}

extension ImagesFeedViewController {
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let pageIndex = indexPath.row
        let viewModel = viewModels[pageIndex]
        if pageIndex >= pagesCount - 1 {
            imagesFeedOutput?.didScrollToEnd()
        }
        
        switch viewModel {
        case let advertisementVm as AdvertisementViewModel:
            let adCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertisementCell", for: indexPath) as! AdvertisementCell
            advertisementOutput?.didShow(advertisementPage: adCell, withViewModel: advertisementVm)
            return adCell
        case let imageVm as ImageViewModel:
            let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesFeedCell", for: indexPath) as! ImagesFeedCell
            imageCell.id = imageVm.imageDescription.name
            imagesFeedOutput?.didShow(imageFeedPage: imageCell, withViewModel: imageVm)
            return imageCell
        default:
            fatalError()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let vm = viewModels[indexPath.row] as? ImageViewModel {
            vm.isViewed = true
            updateNotViewedImagesCountTitle()
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pagesCount
    }
}

extension ImagesFeedViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let inset = collectionView.contentInset
        let bounds = collectionView.bounds
        let width = bounds.width - (inset.left + inset.right)
        let height = bounds.height / 1.5
        return CGSize(width: Int(width), height: Int(height))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
