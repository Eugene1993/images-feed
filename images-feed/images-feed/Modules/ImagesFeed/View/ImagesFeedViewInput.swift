//
//  ImagesFeedViewInput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImagesFeedViewInput: class {
    func showFeed(viewModels: [ViewModelProtocol])
    func appendNewImages(newViewModels: [ImageViewModel])
    func setBoardName(title: String)
    func setLoading(isLoading: Bool)
    func showError(error: String)
}
