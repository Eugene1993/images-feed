//
//  ImageFeedPageViewInput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol ImageFeedPageViewInput {
    var id: String? { get set }
    func showImage(image: Image)
    func setName(name: String)
    func showPreview(preview: Image)
    func setLoading(isLoading: Bool)
    func clear()
}
