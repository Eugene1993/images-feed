//
//  RootRouter.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class RootRouter: RootRouterInput {
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func showImagesFeed() {
        let httpClient = AlamofireHttpClient()
        let threadsService = FourchanThreadsService(httpClient: httpClient)
        let imageService = FourchanImageService(httpClient: httpClient)
        let imagesLoader = NetworkImagesLoader(imageService: imageService)
        let boardsService = FourchanBoardsService(httpClient: httpClient)
        let imagesFeedModule = ImagesFeedModule(imagesLoader: imagesLoader, threadsService: threadsService, boardsService: boardsService)
        let navController = UINavigationController(rootViewController: imagesFeedModule.view)
        self.window.rootViewController = navController
        self.window.makeKeyAndVisible()
    }
}
