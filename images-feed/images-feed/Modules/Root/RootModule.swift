//
//  RootModule.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class RootModule {
    let router: RootRouterInput
    
    init(window: UIWindow) {
        self.router = RootRouter(window: window)
    }
}
