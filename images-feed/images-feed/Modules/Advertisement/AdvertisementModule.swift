//
//  AdvertisementModule.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class AdvertisementModule {
    let presenter: AdvertisementViewOutput
    
    init(advertisementViewInput: AdvertisementViewInput, advertisementService: AdvertisementServiceProtocol) {
        let presenter = AdvertisementViewPresenter()
        let advertisementInteractor = AdvertisementInteractor(advertisementService: advertisementService)
        
        advertisementService.listener = advertisementInteractor
        presenter.interactor = advertisementInteractor
        presenter.view = advertisementViewInput
        advertisementInteractor.output = presenter
        self.presenter = presenter
    }
}
