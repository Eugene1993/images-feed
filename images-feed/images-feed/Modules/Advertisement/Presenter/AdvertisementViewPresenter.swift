//
//  AdvertisementViewPresenter.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class AdvertisementViewPresenter: AdvertisementInteractorOutput, AdvertisementViewOutput {
    private var maxPagesCount = 1000
    weak var view: AdvertisementViewInput?
    var interactor: AdvertisementInteractorInput?
    
    func showAdvertisement(advertisementLink: String) {
        guard let pagesCount = self.view?.pagesCount else {
            return
        }
        
        if pagesCount >= maxPagesCount {
            return
        }
        
        let randomPagePosition = Int.random(in: 0...pagesCount)
        let viewModel = AdvertisementViewModel(link: advertisementLink)
        self.view?.showAdvertisement(viewModel: viewModel, onPage: randomPagePosition)
    }
    
    func didShow(advertisementPage page: AdvertismentFeedPageInput, withViewModel viewModel: AdvertisementViewModel) {
        page.showAdvertisement(advertisementUrl: viewModel.link)
    }
    
    func didLoad() {
        interactor?.startShowingAd()
    }
}
