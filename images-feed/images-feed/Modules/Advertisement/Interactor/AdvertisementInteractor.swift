//
//  AdvertisementInteractor.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class AdvertisementInteractor: AdvertisementServiceListener, AdvertisementInteractorInput {
    let advertisementService: AdvertisementServiceProtocol
    weak var output: AdvertisementInteractorOutput?
    
    init(advertisementService: AdvertisementServiceProtocol) {
        self.advertisementService = advertisementService
    }
    
    func didReceiveAdvertisementLink(link: String) {
        output?.showAdvertisement(advertisementLink: link)
    }
    
    func startShowingAd() {
        advertisementService.start()
    }
    
    func stopShowingAd() {
        advertisementService.stop()
    }
}
