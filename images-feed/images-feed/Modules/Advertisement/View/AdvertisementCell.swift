//
//  AdvertisementCell.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class AdvertisementCell: UICollectionViewCell, AdvertismentFeedPageInput, WKNavigationDelegate {
    var webView: WKWebView?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        setup()
    }
    
    func showAdvertisement(advertisementUrl: String) {
        activityIndicator.startAnimating()
        webView?.isHidden = true
        webView?.stopLoading()
        guard let url = URL(string: advertisementUrl) else {
            return
        }
        
        let request = URLRequest(url: url)
        webView?.load(request)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.isHidden = false
    }
    
    private func setup() {
        let webView = WKWebView()
        webView.isUserInteractionEnabled = false
        contentView.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        webView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0).isActive = true
        webView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        webView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        self.webView = webView
        activityIndicator.tintColor = .appRed
    }
}
