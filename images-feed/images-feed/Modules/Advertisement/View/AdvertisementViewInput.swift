//
//  AdvertisementViewInput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol AdvertisementViewInput: class {
    var pagesCount: Int { get }
    func showAdvertisement(viewModel: AdvertisementViewModel, onPage pageIndex: Int)
}
