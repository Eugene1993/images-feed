//
//  BoardsRouter.swift
//  fourchan
//
//  Created by Заволожанский Евгений on 04/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

class BoardsRouter: BoardsRouterInput {
    weak var controller: BoardsViewController?
    weak var listener: BoardsRouterTransitionListener?
    
    init(controller: BoardsViewController) {
        self.controller = controller
    }
    
    func dismiss() {
        controller?.dismiss(animated: true, completion: nil)
    }
    
    func openBoard(board: Board) {
        dismiss()
        listener?.didOpenBoard(board: board)
    }
}
