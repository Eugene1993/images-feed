//
//  BoardsViewInput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol BoardsViewInput: class {
    func showBoards(viewModels: [BoardViewModel], currentBoard: Board?)
    func setLoading(isLoading: Bool)
    func showError(error: String)
}
