//
//  BoardsViewOutput.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

protocol BoardsViewOutput: class {
    func didSelectBoard(boardViewModel: BoardViewModel)
    func didClose()
    func didLoad()
}
