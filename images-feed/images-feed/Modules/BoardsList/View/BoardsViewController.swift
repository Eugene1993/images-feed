//
//  BoardsViewController.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

final class BoardsViewController: UICollectionViewController, BoardsViewInput {
    var viewModels = [BoardViewModel]()
    var presenter: BoardsViewOutput?
    var currentBoard: Board?
    
    override func viewDidLoad() {
        presenter?.didLoad()
        setupNavigationBar()
        self.collectionView.backgroundColor = UIColor.appBlack
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BoardCell", for: indexPath) as! BoardCell

        let rowNumber = indexPath.row
        let boardVm = viewModels[rowNumber]
        
        var textColor = UIColor.white
        if let currentBoard = currentBoard {
            textColor = boardVm.name == "/\(currentBoard.name)/" ? UIColor.appYellow : UIColor.white
        }
        
        cell.boardId.text = "\(boardVm.name)"
        cell.boardId.textColor = textColor
        cell.boardName.text = boardVm.title
        cell.boardName.textColor = textColor
    
        return cell
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func showBoards(viewModels: [BoardViewModel], currentBoard: Board?) {
        DispatchQueue.main.async {
            self.currentBoard = currentBoard
            self.viewModels = viewModels
            self.collectionView.reloadData()
        }
    }
    
    func setLoading(isLoading: Bool) {
        DispatchQueue.main.async {
            if isLoading {
                SVProgressHUD.show()
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func showError(error: String) {
        DispatchQueue.main.async {
            SVProgressHUD.showError(withStatus: error)
        }
    }
    
    private func setupNavigationBar() {
        guard let navBar = self.navigationController?.navigationBar else {
            return
        }
        
        navBar.barStyle = .blackOpaque
        navBar.barTintColor = .appBlack
        navBar.tintColor = .appLightGray
        navBar.isTranslucent = false
        self.navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(close)), animated: false)
    }
    
    @objc private func close() {
        presenter?.didClose()
    }
}

extension BoardsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let inset = collectionView.contentInset
        let width = collectionView.bounds.width - (inset.left + inset.right)
        let height = 50
        return CGSize(width: Int(width), height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vm = viewModels[indexPath.row]
        presenter?.didSelectBoard(boardViewModel: vm)
    }
}
