//
//  BoardCell.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class BoardCell: UICollectionViewCell {
    @IBOutlet weak var boardName: UILabel!
    @IBOutlet weak var boardId: UILabel!
    
    override func awakeFromNib() {
        boardName.textColor = .white
        boardId.textColor = .white
    }
}
