//
//  BoardsPresenter.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class BoardsPresenter: BoardsViewOutput, BoardsInteractorOutput {
    var interactor: BoardsInteractorInput?
    var router: BoardsRouterInput?
    var currentBoard: Board?
    weak var view: BoardsViewInput?
    
    init(currentBoard: Board?) {
        self.currentBoard = currentBoard
    }
    
    func didLoad() {
        interactor?.loadBoards()
        view?.setLoading(isLoading: true)
    }
    
    func showBoards(boards: [Board]) {
        defer {
            view?.setLoading(isLoading: false)
        }
        
        let viewModels = boards.map { (board) -> BoardViewModel in
            return BoardViewModel(board: board)
        }
        view?.showBoards(viewModels: viewModels, currentBoard: currentBoard)
    }
    
    func didSelectBoard(boardViewModel: BoardViewModel) {
        router?.openBoard(board: boardViewModel.board)
    }
    
    func didClose() {
        router?.dismiss()
    }
    
    func showError(error: String) {
        view?.showError(error: error)
    }
}
