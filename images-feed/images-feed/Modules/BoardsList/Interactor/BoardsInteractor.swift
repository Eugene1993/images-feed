//
//  BoardsInteractor.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation

final class BoardsInteractor: BoardsInteractorInput {
    
    weak var presenter: BoardsInteractorOutput?
    let boardsService: BoardsServiceProtocol
    var boards: [Board]?
    
    init(boardsService: BoardsServiceProtocol) {
        self.boardsService = boardsService
    }
    
    func loadBoards() {
        self.boardsService.fetchBoards { (boards, error) in
            self.boards = boards
            self.presenter?.showBoards(boards: boards)
            if let error = error {
                self.presenter?.showError(error: error.localizedDescription)
            }
        }
    }
}
