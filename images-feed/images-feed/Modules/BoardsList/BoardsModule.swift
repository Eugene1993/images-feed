//
//  BoardsBuilder.swift
//  images-feed
//
//  Created by Заволожанский Евгений on 11/05/2019.
//  Copyright © 2019 Zavolozhanskiy Evgeniy. All rights reserved.
//

import Foundation
import UIKit

class BoardsModule {
    let view: BoardsViewController
    
    init(transitionListener: BoardsRouterTransitionListener, boardsService: BoardsServiceProtocol, currentBoard: Board?) {
        let storyboard = UIStoryboard(name: "BoardsViewController", bundle: nil)
        view = storyboard.instantiateViewController(withIdentifier: "BoardsViewController") as! BoardsViewController
        let interactor = BoardsInteractor(boardsService: boardsService)
        let presenter = BoardsPresenter(currentBoard: currentBoard)
        let router = BoardsRouter(controller: view)
        
        router.listener = transitionListener
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        view.presenter = presenter
        interactor.presenter = presenter
    }
}
