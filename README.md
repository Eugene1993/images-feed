# Тестовое задание для FunCorp iOS Hiring Event

## Запуск

  - Скачать репозиторий
  - Выполнить pod install
  - Открыть images-feed.xcworkspace
  
## Используемый API

  - В задании использовался [публичный 4chan API](https://github.com/4chan/4chan-API)
  
## Используемые библиотеки

  - **Alamofire** для работы с сетью
  - **SwiftyJson** для парсинга JSON
  - **SVProgressHud** - индикатор загрузки